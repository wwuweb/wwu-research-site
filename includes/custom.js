    //
    //This is the feed for the Research RSS.
    //https://westerntoday.wwu.edu/taxonomy/term/4753/all/feed

    //This is the Faculty Research feed.
    //https://westerntoday.wwu.edu/taxonomy/term/4395/all/feed

    loadRss("https://westerntoday.wwu.edu/taxonomy/term/4753/all/feed", document.getElementById("research"), "<h3>Research in the News</h3>", 6);
    loadRss("https://westerntoday.wwu.edu/taxonomy/term/4395/all/feed", document.getElementById("facresearch"), "<h3>Faculty Research</h3>", 6);
    
    
    function loadRss(rssurl, location, header, listLength)
    {
        location.innerHTML = header;
        $.get(rssurl, function(data) {
        var $xml = $(data);
        var i=1;
        $xml.find("item").each(function() {
            var $this = $(this),
                item = {
                    title: $this.find("title").text(),
                    link: $this.find("link").text(),
                    //description: $this.find("description").text(),
                    //pubDate: $this.find("pubDate").text(),
                    //author: $this.find("author").text()
                }

                if (i <= listLength)               
                        location.innerHTML += "<div style='margin-top:15px; line-height:20px;' ><a href='" + item.link + "'>" + item.title + "</a></div>";
                i++;
                
                });
            });
    }

// // // // // // //      Sticky Kit      // // // // // // //
$(".main-nav").stick_in_parent({parent: "body"});  