// // // // // // //      Accordion Navigation      // // // // // // //
//meep
$(".site-nav li").has("ul").addClass("is-accordion is-closed");
$(".site-nav .is-closed > ul").hide();

// if child of ul and it's a parent of another ul
$(".site-nav .is-accordion").click(function(event) {
	event.stopPropagation();
	$(this).toggleClass("is-closed is-open");
	$(this).children("ul").slideToggle();
});


// Prevents $("a").click() from bubbling up to parent <li> if it doesn't have a nested/child accordion
var stopNestedBubbling = $(".site-nav li").not(":has(ul)");

stopNestedBubbling.click(function(event) {
	event.stopPropagation();
});

// //      Active Accordion Anchors and Parents     // //
// Styles for active nav items and nav item parents
var currentLocation = window.location.href.split('/');
var page;
page = currentLocation[currentLocation.length - 1];
subpage = currentLocation[currentLocation.length - 2];

$('a[href*="' + subpage + '/' + page + '"]').addClass("is-active");
$("a[href='#']").click(function(e) {
	e.preventDefault();
});

$(".site-nav .is-active").parents(".is-accordion").addClass("is-active-parent");
$(".site-nav .is-active-parent ul").show();
$(".site-nav .is-active-parent").toggleClass("is-closed is-open");



// // // // // // //      Content Accordions      // // // // // // //

var accordionHeading = $("main .is-accordion");
var accordionContent = $(".is-accordion-content");

$(accordionHeading).addClass("is-closed");
$(accordionContent).hide();

$(accordionHeading).click(function() {
	$(this).next(accordionContent).slideToggle();
	$(this).toggleClass("is-closed is-open");
});



// // // // // // //      Western Icons      // // // // // // //

// //      Button Toggles     // //
var searchButton = $(".western-search > button");
var searchWidget = $(".western-search-widget");

// Search
searchWidget.hide();

searchButton.click(function () {
	searchWidget.animate({width: 'toggle'});
});

// Western Quick Links
var quickLinksButton = $(".western-quick-links > button");
var quickLinksWidget = $(".western-quick-links ul");

quickLinksButton.click(function () {
	quickLinksWidget.animate({width: 'toggle'});
});
/*// if quickLinksButton is display:block; hide widget
if (quickLinksButton.css("display", "")) {
	quickLinksWidget.hide();
}*/

// Mobile Main Navigation
var mainNavButton = $(".mobile-main-nav");
var mainNav = $(".main-nav");

mainNavButton.click(function () {
	mainNav.slideToggle();
});

// // // // // // //      Sticky Kit      // // // // // // //
$(".main-nav").stick_in_parent({parent: "body"});
